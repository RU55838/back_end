console.log("Iniciando....");
var express= require('express');    //Nombre de API remota (la que se consume)
// *** Esto del bodyParser es para parsear lo que enviamos a json
var bodyParser = require('body-parser');
var requestJSON= require('request-json');    //Para conectarse a mLab
var app= express();  //aqui creamos un objeto de la clase express

//Tambien Para conectarse a mLab
var baseMLabURL = 'https://api.mlab.com/api/1/databases/techubduruguay_2/collections/';
var apikeyMLab = 'apiKey=ipzjqmeOFJPp4LodValzyBsUe_zEZi5V';
//https://api.mlab.com/api/1/databases/techubduruguay_2/collections/user?apiKey=ipzjqmeOFJPp4LodValzyBsUe_zEZi5V
// ***** fin mLab

app.use(bodyParser.json());
var port= process.env.PORT || 3000;
const URI= '/api-uruguay/v1/';

//pruebas de Funciones
    //var cambioclave= ChgPassword(8, "hola54321");
    //var saldo= GetSaldo(5);
    //var hayCta= ExisteUsrCta(12, 15);
    //var qmail="wkemsterg@pagesperso-orange.fr";
    //var hayEmail= ExisteEMail(qmail);
    //var F1= new Date();
    //var insertMov= putNewMvto(5, F1, "Tienda Mia", 13450);

//var newIDUser=0;
//****************************************************************************
//**    F U N C I O N E S    *************************************************
//****************************************************************************
function zzCargaParametros() {
   return "HolaMundo";
};



function ExisteUsrCta(pIdUSR, pIdCuenta) {
//Valida Si el nùmero de cuenta solicitado pertenece al usuario
  var httpClient = requestJSON.createClient(baseMLabURL);
  var queryString = 'q={"id":' + pIdUSR + '}&';
  var queryStrField = 'f={"_id":0}&';
  var Existe=false;
  httpClient.get('users?' + queryString + queryStrField + apikeyMLab,
    function(err, respuestaMLab, body) {
      if(!err) {
        if(body.length > 0) {
          console.log('CUENTAS DEL USUARIO >>  id=' + body[0].id + "  OldPass= " + body[0].password);
          if (body[0].id != undefined) {
                var cuentas= body[0].accounts;
                console.log("+++Cuentas ");
                for (i=0; i < cuentas.length; ++i){
                    console.log("    Cuenta:: " + cuentas[i].IdAccount)
                    if (cuentas[i].IdAccount === pIdCuenta) {
                       console.log("Usuario-Cuenta(" + pIdUSR + "/" + pIdCuenta + ") existe!");
                       Existe= true;
                       break;
                    }
                }
                if (!Existe){
                  console.log("Usuario-Cuenta(" + pIdUSR + "/" + pIdCuenta + ") NO existe!");
                }
          } //if
        }
        else {
          console.log("Usuario-Cuenta(" + pIdUSR + ") NO ENCONTRADO!");
        }
      }
      else {
        console.log("Ha ocurrido un error para obtener las cuentas del Usuario # " + pIdUSR);
      }
    });
  return Existe;
};

//****************************************************************************
//**    S E R V I C I O S   API-REST    **************************************
//****************************************************************************

// Petición userscuentas- retorna las cuentas pertenecientes a un usuario
// http://localhost:3000/api-uruguay/v1/userscuentas/7
app.get(URI + 'userscuentas/:id',
  function (req, res) {
    console.log(req.params.id);
    var id = req.params.id;
    var queryString = 'q={"id":' + id + '}&';
    var queryStrField = 'f={"_id":0}&';
    var httpClient = requestJSON.createClient(baseMLabURL);

    httpClient.get('users?' + queryString + queryStrField + apikeyMLab,
      function(err, respuestaMLab, body){
        var response = {};
        if(err) {
            console.log("Error obteniendo cuentas de usuario.");
            response = { "msg" : "Error obteniendo cuentas de usuario." }
            res.status(204);
        } else {
          if(body.length > 0) {
            response = body[0].accounts;
          } else {
            console.log("Usuario no encontrado.");
            response = {"msg" : "Usuario no encontrado." }
            res.status(204);
          }
        }
        res.send(response);
      });
});


// Petición mvtoscuenta- retorna los movimientos de una cuenta
// http://localhost:3000/api-uruguay/v1/mvtoscuenta/7
app.get(URI + 'mvtoscuenta/:id',
  function (req, res) {
    console.log(req.params.id);
    var id = req.params.id;
    var queryString = 'q={"idCuenta":' + id + '}&';
    var queryStrField = 'f={"_id":0}&';
    var httpClient = requestJSON.createClient(baseMLabURL);

    httpClient.get('cuentas?' + queryString + queryStrField + apikeyMLab,
      function(err, respuestaMLab, body){
        var response = {};
        if(err) {
            console.log("Error obteniendo movimientos de cuenta.");
            response = { "msg" : "Error obteniendo movimientos de cuenta." }
            res.status(204);
        } else {
          if(body.length > 0) {
            response = body[0];
          } else {
            console.log("Cuenta no encontrada.");
            response = {"msg" : "Cuenta no encontrada." }
            res.status(204);
          }
        }
        res.send(response);
      });
});


//Metodo get existeMail
//http://localhost:3000/api-uruguay/v1/existeMail/lheaslipb@liveinternet.ru
app.get(URI + "existeMail/:email",
  function (req, res){
//Verifica si existe una direcciòn de correo
  var httpClient = requestJSON.createClient(baseMLabURL);
  var pEMail = req.params.email;
  var queryString = 'q={"email":"' + pEMail + '"}&';
  var queryStrField = 'f={"_email":0}&';
  var ExisteMail=false;
  console.log("pEMail=" + pEMail);
  httpClient.get('users?' + queryString + queryStrField + apikeyMLab,
    function(err, respuestaMLab, body) {
      if(!err) {
        if(body.length > 0) {
          if (body[0].id != undefined) {
            for (i=0; i < body.length; ++i){
                if (body[i].email === pEMail) {
                   ExisteMail= true;
                   console.log("eMail(" + pEMail + ") existe!");
                   res.status(200);
                   res.send({"Existe":"SI"});
                   break;
                }
            }
            if (!ExisteMail){
              console.log("eMail(" + pEMail + ") NO existe!");
              res.status(200);
              res.send({"Existe":"NO"});
            }
          }
        }
        else {
          console.log("eMail (" + pEMail + ") NO ENCONTRADO!");
          res.status(200);
          res.send({"Existe":"NO"});
        }
      }
      else {
        console.log("Ha ocurrido un error para obtener el eMail de usuarios.");
        res.status(204);
        res.send({"Mensaje":"Ha ocurrido un error para obtener el eMail de usuarios."});
      }
    });
});


//Metodo POST login
//http://localhost:3000/api-uruguay/v1/login/
//   { "email": "kendley5@japanpost.jp2", "password": "LGJM2u7z" }
app.post(URI + "login",
  function (req, res){
    console.log("Accediendo al mètodo POST login");
    var email = req.body.email;
    var pass = req.body.password;
    var queryStringEmail = 'q={"email":"' + email + '"}&';
    var queryStringpass = 'q={"password":' + pass + '}&';
    var  clienteMlab = requestJSON.createClient(baseMLabURL);
    clienteMlab.get('users?'+ queryStringEmail + apikeyMLab ,
    function(error, respuestaMLab , body) {
      //console.log(">> Entro al body:" + body );
      var respuesta = body[0];
      console.log(respuesta);
      if(respuesta != undefined){
          if (respuesta.password === pass) {
            console.log("Login Correcto");
            var session = {"logged":true};
            var login = '{"$set":' + JSON.stringify(session) + '}';
            console.log(baseMLabURL+'?q={"id": ' + respuesta.id + '}&' + apikeyMLab);
            clienteMlab.put('user?q={"id": ' + respuesta.id + '}&' + apikeyMLab, JSON.parse(login),
              function(errorP, respuestaMLabP, bodyP) {
                res.send(body[0]);
              });
          }
          else {
            console.log("Contraseña incorrecta");
            res.status(401);
            res.send({"msg":"Alguno de los datos ingresados no es correcto, verifique."});
          }
      } else {
        console.log("Email Incorrecto");
        res.status(401);
        res.send({"msg": "Alguno de los datos ingresados no es correcto, verifique."});
      }
    });
});



//Method POST logout
//http://localhost:3000/api-uruguay/v1/logout/
//Paràmetros en body como json  = ({ "email": "kendley5@japanpost.jp", "password": "LGJM2u7z" })
app.post(URI + "logout",
  function (req, res){
    console.log("ACCEDIENDO AL METODO POST logout...");
    var email = req.body.email;
    var queryStringEmail = 'q={"email":"' + email + '"}&';
    //Luego borrar, es para validar si me llegan bien los datos
        console.log("Email Recibido Logout=" + queryStringEmail);
    //Fin Luego borrar
    var  clienteMlab = requestJSON.createClient(baseMLabURL);
    clienteMlab.get('users?'+ queryStringEmail + apikeyMLab ,
    function(error, respuestaMLab, body) {
      console.log(">>>Entro al get");
      var respuesta = body[0];
      console.log(respuesta);
      if(respuesta != undefined){
            console.log("Logout Correcto");
            var session = {"logged":false};
            var logout = '{"$unset":' + JSON.stringify(session) + '}';
            console.log(logout);

            clienteMlab.put('users?q={"id": ' + respuesta.id + '}&' + apikeyMLab, JSON.parse(logout),
              function(errorP, respuestaMLabP, bodyP) {
                res.send(body[0]);
                //res.send({"msg": "logout Exitoso"});
                console.log("Logout Exitoso...");
              });
      } else {
        res.status(401);
        console.log("Error en logout");
        res.send({"msg": "No se ha podido realizar el Logout"});
      }
    });
});


//METODO PUT (UPDATE)- CAMBIAR LA CONTRASEÑA DE UN USUARIO LOGUEADO
//http://localhost:3000/api-uruguay/v1/cambiarpwd
// por body { "idUser": 5, "oldpwd": "laclavevieja" "pwd": "nuevaclave" }
app.put(URI + "cambiarpwd",
  function(req, res){
    console.log('Ejecutando petición PUT /api-uruguay/v1/cambiarPwd.');
    var pUSR = req.body.idUser;
    var nuevaPWD = req.body.pwd;
    var oldPWD = req.body.oldpwd;
    console.log("Id usuario a actualizar: " + pUSR + " - " + oldPWD  + " - " + nuevaPWD);
    var httpClient = requestJSON.createClient(baseMLabURL);
    var queryString = 'q={"id":' + pUSR + '}&';
    var queryStrField = 'f={"_id":0}&';
    var PwdActualizada=0;

    httpClient.get('users?' + queryString + queryStrField + apikeyMLab,
      function(err, respuestaMLab, body) {
        if(!err) {
          if(body.length > 0) {
            console.log('CHANGE PASSWORD >>  id=' + body[0].id + "  OldPass= " + body[0].password);
            if (body[0].id != undefined) {
              if (body[0].password === oldPWD) {
                console.log("body[0].first_name== "+body[0].first_name);
                var newPWD = {
                  'id': +pUSR,
                  'first_name': body[0].first_name,
                  'last_name': body[0].last_name,
                  'email': body[0].email,
                  'password': nuevaPWD,
                  'accounts': body[0].accounts
                };
                PwdActualizada= 1;  //true
                console.log("Encontre ID, modifico PWD..." + PwdActualizada);
                httpClient.put('users?q={"id": ' + pUSR + '}&' + apikeyMLab, newPWD,
                  function(error, respuestaMLab,body){
                    if (respuestaMLab != undefined){
                       PwdActualizada= 1;  //true
                       console.log("Password modificada con èxito..");
                       res.send({"msg":"Contraseña actualizada exitosamente."});
                    }
                  });
              } //OldPass
              else {
                console.log("oldPWD incorrecta.1");
                res.status(404);
                res.send({"msg":"Alguno de los datos ingresados es incorrecto."});
              }
            }//If
            else{
              console.log("oldPWD incorrecta.");
              res.status(404);
              res.send({"msg":"Alguno de los datos ingresados es incorrecto."});
            }
          }
          else {
            console.log("Usuario # " + pUSR + " no encontrado.");
            res.status(404);
            res.send({"msg":"El Id de usuario a actualizar no existe."});
          }
        }
        else {
          console.log("Ha ocurrido un error para obtener el Usuario # " + pUSR);
          res.status(404);
          res.send({"msg":"El Id de usuario a actualizar no existe."});
        }
      });
  });

  // METODO POST (Create)  --  REGISTRAR  UN NUEVO USUARIO
  //http://localhost:3000/api-uruguay/v1/nuevousuario/
  //En Body se pone: {"first_name":"Nissa992","last_name":"Guthrum","email":"nguthrum0@google.pl","password":"gFiAmND95", "accounts": [ { "IdAccount": 177 }, { "IdAccount": 288 } ] }
  app.post(URI + 'nuevousuario',
    function(req, res){
      var clienteMlab = requestJSON.createClient(baseMLabURL);
      //var newID = GetNewID();
      //console.log(">>>>>>>>>>>>>>>>>>>>>>>>" + newID + newIDUser);
      //*************************************************************
      //Retorna nuevo nùmero de usuario
        var httpClient = requestJSON.createClient(baseMLabURL);
        var queryString = 'f={"_id":0}&';
        var newIDUser=0;

        httpClient.get('parametros?' + queryString + apikeyMLab,
          function(err, respuestaMLab, body) {
            var response = {};
            if(!err) {
              if(body.length > 0) {
                //Busco e incremento el numerador de Usuarios
                for (i=0; i < body.length; ++i)
                {
                    if (body[i].num_id != undefined) {
                      console.log("Contador= " + body[i].num_id);
                      var IDUser= body[i].num_id;
                      newIDUser= IDUser + 1;
                      var regIdUser = { 'num_id':newIDUser };

                      console.log("Encontre Contador, lo incremento...");
                      httpClient.put('parametros?q={"num_id": ' + IDUser + '}&' + apikeyMLab, regIdUser,
                        function(error, respuestaMLab,body){
                          if (respuestaMLab != undefined){
                            console.log("Parametro #num_id# incrementado correctamente..." + newIDUser);
                            //Comienzo a dar de alta al nuevo usuario
                            console.log("req.body.accounts=" + req.body.accounts);
                             var newUser = {
                               "id" : newIDUser,
                               "first_name" : req.body.first_name,
                               "last_name" : req.body.last_name,
                               "email" : req.body.email,
                               "password" : req.body.password,
                               'accounts':req.body.accounts    //Aqui ver como Parsearlo
                             };

                            clienteMlab.post(baseMLabURL + "users?" + apikeyMLab, newUser,
                              function(error, respuestaMLab, body){
                                if (respuestaMLab != undefined){
                                   console.log("Usuario #" + newIDUser + " dado de alta correctamente.");
                                   res.send(body);
                                }
                                else {
                                  console.log("ATENCION: No se ha podido dar de alta!");
                                  res.status(404);
                                  res.send({"msg":"ATENCION: No se ha podido dar de alta!"});
                                }
                              });
                          }
                        });
                      break;
                    }
                } //for
              }
              else {
                console.log("Parametros no encontrados.");
              }
            }
          });
  });




// METODO POST (Create)  --  INGRESAR UN MOVIMIENTO
//http://localhost:3000/api-uruguay/v1/altamovimiento
//En Body se pone: { "cuenta": 1, "fecha": "2018-04-06T01:30:52Z", "descripcion": "mov1", "importe": 69001 }
app.post(URI + "altamovimiento",
  function(req, res){
    console.log('Ejecutando petición PUT /api-uruguay/v1/altamovimiento.');
    var pIdCuenta = req.body.cuenta;
    var p_fecha = req.body.fecha;
    var p_desc= req.body.descripcion;
    var p_imp= req.body.importe;
    console.log("NuevoMvto: " + pIdCuenta + " - " + p_fecha + " - "  + p_desc +  " - " + p_imp);
    //Antes de ingresar el dèbito, reviso que tenga saldo
    var httpClient = requestJSON.createClient(baseMLabURL);
    var queryString = 'q={"idCuenta":' + pIdCuenta + '}&';
    var queryStrField = 'f={"_id":0}&';
    //var SaldoAcutal=0;
    httpClient.get('cuentas?' + queryString + queryStrField + apikeyMLab,
      function(err, respuestaMLab, body) {
        if(!err) {
          if(body.length > 0) {
            var SaldoAcutal= +body[0].saldo;
            console.log("SaldoActual=" + SaldoAcutal);

            if (SaldoAcutal > 0) {
              //#### NUEVO Movimiento
              console.log("Pronto para hacer el NEW !!!")
              var allMovs= body[0].movimientos;
              var newIdMov= 0;
              var nueMOV, ctaMov;
              //Busco un nuevo id Movimiento
              if (allMovs.length > 0) {
                for (i=0; i < allMovs.length; ++i){
                   if (allMovs[i].idmov > newIdMov ){
                     newIdMov= allMovs[i].idmov;
                   }
                }
                newIdMov+=1;
              }
              else {
                newIdMov=1;
              }

              //Json nuevo mvto
              nueMOV= {
                  'idmov': newIdMov,
                  'fecha': p_fecha,
                  'descripcion': p_desc,
                  'importe': p_imp
              };

              ctaMov=body[0];
              console.log("GGGGGGgggg" + p_imp + "--->" + SaldoAcutal)
              SaldoAcutal=+SaldoAcutal + p_imp;
/*              if (p_imp > 0){
                SaldoAcutal+=p_imp;
              }
              else{
                SaldoAcutal-=(p_imp * -1);
              }
*/
              ctaMov.saldo= SaldoAcutal;
              ctaMov.movimientos.push(nueMOV);

              //Grabo el Movimiento
              httpClient.put('cuentas?q={"idCuenta": ' + pIdCuenta + '}&' + apikeyMLab, ctaMov,
                function(error, respuestaMLab_2,body){
                  if (respuestaMLab_2 != undefined){
                     newMvto=true;
                     console.log("Movimiento Agregado con èxito.!!!   Nuevo Saldo= " + SaldoAcutal);
                     res.status(201);
                     res.send({"msg":"Movimiento Agregado con èxito.!!!"});
                  }
                  else {
                    console.log(">>Atencion: No se ha podido agregar el nuevo Movimiento.");
                    res.status(204);
                    res.send({"msg":"Atencion: No se ha podido agregar el nuevo Movimiento."});
                  }
                });
              //Fin Grabo Movimiento
            }
            else{
              res.status(204);
              res.send({"msg":"Atencion: SALDO INSUFICIENTE. Movimiento No Realizado."});
            }
          }
          else {
            console.log("Cuenta # " + pIdCuenta + " no encontrada.");
            res.status(204);
            res.send({"msg":"Cuenta # " + pIdCuenta + " no encontrada."});
          }
        }
        else {
          console.log("Ha ocurrido un error al tratar de obtener la cuenta# " + pIdCuenta);
          res.status(204);
          res.send({"msg":"Ha ocurrido un error al tratar de obtener la cuenta# " + pIdCuenta });
        }
      });

});

app.listen(port);
console.log("Escuchando en el puerto 3000  mybankapp_batch Uruguay - JavierFraga....");
