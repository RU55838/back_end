#Comentarios

# Imagen Docker base  cuya etiqueta es la publicada en el catalogo (latest)
FROM node:latest

#Directorio de trabajo de docker
WORKDIR /docker-api

#Copiar archivos del proyecto al directorio que se ha creado en el paso anterior en WORKDIR
ADD . /docker-api

#Aqui iria el comando= RUN npm install si se desea que se instalen las dependencias
#RUN npm install

#Puerto donde exponemos el contenedor
EXPOSE 3000


#Comando para lanzar la app  aqui va el mismo comando.
#El start refiere a lo definido en el archivo package.json
CMD ["npm", "start"]
